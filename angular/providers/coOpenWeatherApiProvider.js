(function() {
  angular
    .module("co-web-apis")
    .provider("OpenWeatherApiProvider", OpenWeatherApiProvider);

  function OpenWeatherApiProvider() {
    let that = this;
    this.apiKey = "SET_API_KEY_IN_PROVIDER_CONFIGURATION";

    this.setApiKey = key => {
      that.apiKey = key;
    };

    this.getApiKey = () => this.apiKey;

    this.$get = [
      "$http",
      $http => {
        return {
          getWeatherFromServer(
            lonLeft = -180,
            latBottom = -90,
            lonRight = 180,
            latTop = 90,
            zoom = 10
          ) {
            return $http
              .get(
                `https://api.openweathermap.org/data/2.5/box/city?bbox=${lonLeft},${latBottom},${lonRight},${latTop},${zoom}&APPID=${that.getApiKey()}`
              )
              .then(response => {
                return response.data;
              });
          }
        };
      }
    ];
  }
})();

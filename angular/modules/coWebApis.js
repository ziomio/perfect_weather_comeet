(function() {
  angular.module("co-web-apis", []).config([
    "OpenWeatherApiProviderProvider",
    function(OpenWeatherApiProviderProvider) {
      OpenWeatherApiProviderProvider.setApiKey(
        "c858777b848b8a1ad59ec9fc32f4c9fb"
      );
    }
  ]);
})();

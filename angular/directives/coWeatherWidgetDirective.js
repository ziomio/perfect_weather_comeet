(function() {
  angular
    .module("co-widgets")
    .directive("coWeatherWidget", coWeatherWidgetDirective);

  function coWeatherWidgetDirective() {
    return {
      restrict: "EA",
      scope: {},
      replace: false,
      templateUrl: "angular/templates/weather_widget_directive.html",
      controller: WeatherWidgetController,
      controllerAs: "vm"
    };
  }

  WeatherWidgetController.$inject = ["$scope", "OpenWeatherApiProvider"];
  function WeatherWidgetController($scope, OpenWeatherApiProvider) {
    let vm = this;

    vm.weather = {
      temperature: 21, // in celcius
      humidity: 50 // in percents
    };

    activate();
    /////////////

    function activate() {
      console.log("WeatherWidgetController: activate");

      vm.isLoading = true;
      OpenWeatherApiProvider.getWeatherFromServer().then(data => {
        vm.isLoading = false;
        vm.bestStations = extractBestStations(data, 50);
      });
    }

    function extractBestStations(data, stationsAmount = 20) {
      function compareStations(stationOne, stationTwo) {
        const tempOne = stationOne.main.temp;
        const tempTwo = stationTwo.main.temp;
        const humidOne = stationOne.main.humidity;
        const humidTwo = stationTwo.main.humidity;

        if (tempOne < tempTwo) {
          return -1;
        } else if (tempOne > tempTwo) {
          return 1;
        } else if (tempOne == tempTwo) {
          // temperatures are equal, so we need to compare humidity level
          if (humidOne < humidTwo) {
            return -1;
          } else if (humidOne > humidTwo) {
            return 1;
          }
          return 0;
        }
      }

      return data.list
        .filter(station => {
          return (
            station.main.temp >= vm.weather.temperature &&
            station.main.humidity >= vm.weather.humidity
          );
        })
        .sort(compareStations)
        .slice(0, stationsAmount);
    }
  }
})();
